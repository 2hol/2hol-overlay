import { Component, JSX } from "solid-js";
import createDimensions from "./createDimensions";
import createRandomActivation from "./createRandomActivation";
import farFromMiddle from "./farFromMiddle";

const Splash: Component<
  JSX.ImgHTMLAttributes<HTMLImageElement> & {
    probability: number;
    delay: number;
  }
> = (props) => {
  const [width, height]: [number, number] = createDimensions();
  const getActive = createRandomActivation(props.probability, props.delay);
  return (
    <>
      {getActive() ? (
        <img
          {...props}
          class="splash"
          style={{
            top: `${Math.random() * height}px`,
            left: `${farFromMiddle(Math.random()) * width}px`,
            transform: `translate(-50%, -50%) scale(${1 + Math.random() * 4})`,
          }}
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default Splash;
