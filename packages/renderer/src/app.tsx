import { Component, For } from "solid-js";
import Particle from "./Particle";
import Splash from "./Splash";

const App: Component = () => (
  <>
    <video autoplay width="60%" class="floating" loop>
      <source
        src="https://gitlab.com/2hol/2hol-assets/-/raw/main/videos/prank.mp4"
        type="video/mp4"
      />
    </video>
    <For each={Array.from({ length: 1 })}>
      {(_, i) => (
        <Particle
          width={"90px"}
          speed={200}
          seed={i() + 900}
          src="https://gitlab.com/2hol/2hol-assets/-/raw/main/images/pill1.png"
        />
      )}
    </For>
    <For each={Array.from({ length: 1 })}>
      {(_, i) => (
        <Particle
          width={"180px"}
          speed={200}
          seed={i() + 789}
          src="https://gitlab.com/2hol/2hol-assets/-/raw/main/images/m3.png"
        />
      )}
    </For>
    <For each={Array.from({ length: 40 })}>
      {(_, i) => (
        <Splash
          width={"150px"}
          probability={0.5}
          delay={1.3708}
          src="https://gitlab.com/2hol/2hol-assets/-/raw/main/images/cum.gif"
        />
      )}
    </For>
    <img
      src="https://gitlab.com/2hol/2hol-assets/-/raw/main/images/cancermsg.png"
      width={"15%"}
      style={{
        position: "absolute",
        transform: "translate(-50%, 0%)",
        left: "50%",
        top: "0",
      }}
    ></img>
  </>
);

export default App;
