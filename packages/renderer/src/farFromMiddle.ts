const farFromMiddle = (x: number, a = 0.35) =>
  x < 0.5 ? a * x : a * (x - 1) + 1;

export default farFromMiddle;
