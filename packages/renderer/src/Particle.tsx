import { Component, createEffect, JSX } from "solid-js";
import createParticleTranslate from "./createParticleTranslate";

const Particle: Component<
  JSX.ImgHTMLAttributes<HTMLImageElement> & { speed: number; seed: number }
> = (props) => {
  const getTranslate = createParticleTranslate(props.speed, props.seed);
  return (
    <img
      {...props}
      class="particle"
      style={{ top: `${getTranslate()[1]}px`, left: `${getTranslate()[0]}px` }}
    />
  );
};

export default Particle;
