const createDimensions = (): [number, number] => {
  const width = window.innerWidth;
  const height = window.innerHeight;
  return [width, height];
};

export default createDimensions;
