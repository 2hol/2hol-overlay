import { createSignal, onCleanup, onMount } from "solid-js";
import createDimensions from "./createDimensions";
import farFromMiddle from "./farFromMiddle";

const offset = 500;

const createParticleTranslate = (speed = 100, seed = 0) => {
  const [getTranslate, setTranslate] = createSignal<[number, number]>([0, 0]);
  const [width, height]: [number, number] = createDimensions();
  let interval: NodeJS.Timer;
  onMount(() => {
    const tick = () => {
      const now = Date.now() * 1e-3;
      setTranslate(() => [
        width * farFromMiddle(((1e3 * seed + speed) % width) / width),
        -offset + ((1e3 * seed + speed * now) % (height + 2 * offset)),
      ]);
    };
    interval = setInterval(tick);
  });
  onCleanup(() => {
    if (interval !== undefined) clearInterval(interval);
  });
  return getTranslate;
};

export default createParticleTranslate;
