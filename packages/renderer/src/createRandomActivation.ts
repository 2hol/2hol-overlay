import { onMount, onCleanup, createSignal } from "solid-js";

const createRandomActivation = (probabilty = 0.1, delay = 1) => {
  const [getActive, setActive] = createSignal<boolean>(false);
  let interval: NodeJS.Timer;
  onMount(() => {
    const tick = () => setActive(Math.random() < probabilty);
    interval = setInterval(tick, 1e3 * delay);
  });
  onCleanup(() => {
    if (interval !== undefined) clearInterval(interval);
  });
  return getActive;
};

export default createRandomActivation;
